//
//  DetailViewController.swift
//  JonatansCV
//
//  Created by Debora Johansson on 12/5/19.
//  Copyright © 2019 Jonatan Larsson. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    
    @IBOutlet weak var detailImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var navigationTitle: UINavigationItem!
    
    
    var experience: Experience = Experience()
       

    override func viewDidLoad() {
        super.viewDidLoad()
    
        navigationTitle.title = self.experience.name
        detailImage.image = UIImage(named: experience.imageName)
        descriptionLabel.text = self.experience.description
        nameLabel.text = self.experience.name
        dateLabel.text = self.experience.date

    }
}
