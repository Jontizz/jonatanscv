//
//  ExperienceClass.swift
//  JonatansCV
//
//  Created by Debora Johansson on 12/5/19.
//  Copyright © 2019 Jonatan Larsson. All rights reserved.
//

import Foundation

struct Experience {
    
    let imageName: String
    let name: String
    let date: String
    let description: String
    
    init(imageName: String = "" , name: String = "", description: String = "", date: String = "") {
        
        self.imageName = imageName
        self.name = name
        self.description = description
        self.date = date
    }
}
