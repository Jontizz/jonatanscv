//
//  ExperienceTableViewCell.swift
//  JonatansCV
//
//  Created by Jonatan Larsson on 12/5/19.
//  Copyright © 2019 Jonatan Larsson. All rights reserved.
//

import UIKit

class ExperienceTableViewCell: UITableViewCell {
    
    @IBOutlet weak var experienceName: UILabel!
    @IBOutlet weak var experienceDate: UILabel!
    @IBOutlet weak var experienceImage: UIImageView!
    

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    

}
