//
//  ExperienceViewController.swift
//  JonatansCV
//
//  Created by Jonatan Larsson on 12/5/19.
//  Copyright © 2019 Jonatan Larsson. All rights reserved.
//

import UIKit

class ExperienceViewController: UIViewController {
    
    var items: Array<Array<Experience>> = [[],[]]
    var sections = ["Work", "Education"]

    @IBOutlet weak var experienceList: UITableView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        experienceList.dataSource = self
        experienceList.delegate = self

        
        loadWorkSource()
        

    }
    
    
    func loadWorkSource() {
           
        items[0].append(Experience(imageName: "aditroImage" , name: "Aditro", description: " Aditro is a warehouse at Torsvik that ships goods to various suppliers around the country. I worked at Aditro for 2 years and did learn alot, like  try different machines and pick goods to different suppliers.", date: "2016 - 2018"))
        items[0].append(Experience(imageName: "beSafeImage", name: "HTS Safety", description: "HTS Safety is a small warehouse at Hedenstorp, which sends car seats and various parts to companies and individuals. Here I work from time to time when I don't have much at shcool.", date: "Current"))
        items[0].append(Experience(imageName: "swedecoteImage", name: "Swedecote", description: "Swedecote is a company that applies different protection to carparts using zinc and various chemicals. I worked night here for about 1 year, but it did not work in the long run.", date: "2015 - 2016"))
        items[1].append(Experience(imageName: "JULogo", name: "JU", description: "I am currently studying the second year at Jönköping University, on the software development and mobile platforms program.", date: "Current"))
        items[1].append(Experience(imageName: "sandaLogo", name: "Sanda", description: "Here I spent my years in high school with economics as my direction. At the same time I practiced football here.", date: "2012 - 2015"))
           
       }
}


extension ExperienceViewController: UITableViewDataSource, UITableViewDelegate {
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        return self.items[section].count
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return sections.count
    }
    
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
          
        return self.sections[section]
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
          
          if let destination = segue.destination as? DetailViewController, let indexPath = sender as? IndexPath {
                  
              destination.experience = self.items[indexPath.section][indexPath.row]
              
          }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        performSegue(withIdentifier: "showExperience", sender: indexPath)
    }
 
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if let experience = tableView.dequeueReusableCell(withIdentifier: "experienceCell", for: indexPath) as? ExperienceTableViewCell{
            let experiences = self.items[indexPath.section][indexPath.row]
            experience.experienceName.text = experiences.name
            experience.experienceDate.text = experiences.date
            experience.experienceImage.image = UIImage(named: experiences.imageName)
            
            return experience
            
        }
     
        return UITableViewCell()
 
    }
   
}


