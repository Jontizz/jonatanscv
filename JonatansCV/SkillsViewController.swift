//
//  SkillsViewController.swift
//  JonatansCV
//
//  Created by Debora Johansson on 11/19/19.
//  Copyright © 2019 Jonatan Larsson. All rights reserved.
//

import UIKit



class SkillsViewController: UIViewController {
    
    
    @IBOutlet weak var dismissButton: UIButton!
    @IBOutlet weak var skillView: UIView!
    
    let duration: Double = 2.0
    
    
    @IBAction func dismissButtonPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        dismissButton.layer.cornerRadius = dismissButton.frame.size.width/2
        
    }
    
    func moveRight(view: UIView) {
        view.center.x += 240
    }
    
    func moveLeft(view: UIView) {
        view.center.x -= 250
    }
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        UIView.animate(withDuration: duration, delay: 1.2, options: .curveEaseIn, animations: {
            self.skillView.backgroundColor = .red
            self.dismissButton.backgroundColor = .blue
            self.moveRight(view: self.skillView)
            self.moveLeft(view: self.dismissButton)
            
        }  , completion: { finished in
            print("Animation finished!")
                
        })
    }
}
